require('../../bookExercises/javascriptTheGoodParts/applyInvocationPattern')


let Stack = function() {
    this.count = 0;
    this.store = {};

    this.push = function(value) {
        this.store[this.count++] = value;
        console.log("push ~> " + value)
    }

    this.pop = function() {
        let result = this.store[--this.count];
        delete this.store[this.count];
        console.log("pop ~> " + result)
        return result;
    }

    this.size = function() {
        return this.count;
    }

    this.peek = function() {
        return "peek ~> " + this.store[this.count - 1];
    }
}


let testStack = new Stack();
testStack.push(7);
console.log("size ~> " + testStack.size());
console.log(testStack.pop());
console.log("size ~> " + testStack.size());
testStack.push("test");
testStack.push(1234);
testStack.push(true);
console.log(testStack.peek());
console.log("size ~> " + testStack.size());