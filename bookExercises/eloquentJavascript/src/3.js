function min(a, ...b) {
    // return (a < b) ? a : b;

    // TODO handle equality case, NaN and ??negatives
    let numberA = Number(a);
    let numberB = Number(b);
    if(numberA < numberB) {
        return numberA;
    } else {
        return numberB;
    }
}


console.log(min(3, 4))