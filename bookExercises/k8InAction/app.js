const http = require('http')
const os = require('os');



const PORT = process.env.PORT || 8787;

const getHandler = (req, res) => res.end(`Welcome to ${os.hostname()}`);
const server = http.createServer(getHandler)
server.listen(PORT, console.log(`Server listening on port ${PORT} @ ${os.hostname()}`));