import axios from 'axios';

let dataFetcher = (function() {

    // function processResponse(error, response, body) {
    //     if(error) {
    //         return Promise.reject('Error processing response');
    //     } else {
    //         return Promise.resolve(body.data);
    //     }
    // }

    function getBaseUrl() {
        return 'http://jsonplaceholder.typicode.com/';
    }

    function getUserUrl() {
        return getBaseUrl()  + 'users/';
    }

    function getDataFromWeb() {
        return new Promise((resolve, reject) => {
            try {
                let data = axios.get(getUserUrl()).then(res => {return res.data});
                resolve(data);
            } catch (error) {
                reject(error);
            }
        });
    }

    return {
        getData : getDataFromWeb
    }
})();

export default dataFetcher;
