let simpleModule = (function () {
    let var1 = "test";

    return {
        getVarOne : function() {
            return var1;
        }
    }
})();

export default simpleModule; // Node did not work with curly braces. Also required 'default' keyword