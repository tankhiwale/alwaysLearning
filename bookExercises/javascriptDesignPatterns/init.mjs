import simpleModule from './moduleDesignPattern.mjs';
import singletonOne from './singleton.mjs';
import dataFetcher from './promises.mjs';


// On how this file ran, see https://stackoverflow.com/a/50641589/984471
// e.g. node --experimental-modules bookExercises/javascriptDesignPatterns/init.mjs

console.log(simpleModule.getVarOne());

dataFetcher.getData().then(retVal =>  { 
        retVal.forEach((value, index) => {
            console.log(value.name);
        });
    }
);

console.log(singletonOne.getInstance().getRandom());
console.log(singletonOne.getInstance().getRandom()); //This should be same as above random number
