let singletonOne = (function(){

    let instance;

    function init(){
        let somePrivateVar = Math.random();

        return {
            getRandom : function() {
                return somePrivateVar;
            }
        };
    };

    return {
        getInstance : function() {
            if(!instance) {
                instance = init();
            }
            return instance;
        }
    };
})();
export default singletonOne; // Node did not work with curly braces. Also required 'default' keyword