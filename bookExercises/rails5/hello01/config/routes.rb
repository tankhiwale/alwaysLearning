Rails.application.routes.draw do
  get 'hello' => 'controller01#index'

  get 'sign_in' => 'entries#sign_in'
  post 'sign_in' => 'entries#sign_in'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end