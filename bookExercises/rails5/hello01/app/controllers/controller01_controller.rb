class Controller01Controller < ApplicationController
  def index
    @title = 'Controller01 title'
    suffix = anotherMothod 'Contro', 'ller'
    @result = 'Controller01' + suffix
    @count = 3
  end

  private
  
  def anotherMothod(a, b)
    a + b
  end
end