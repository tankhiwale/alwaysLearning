var statusObject = {
    status : "statusOne",

    getStatus : function () {
        return this.status;
    }
}

var newStatusObject = {
    status : "statusTwo"
}

console.log("Apply Invocation Pattern ~> " + 
    statusObject.getStatus.apply(newStatusObject));